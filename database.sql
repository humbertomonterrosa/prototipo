SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE DATABASE `prototipo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prototipo`;

CREATE TABLE IF NOT EXISTS `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cumpleano` date NOT NULL,
  `rol` int(11) NOT NULL,
  PRIMARY KEY (`idcliente`),
  KEY `rol` (`rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `roles` (
  `idrol` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) NOT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `roles` (`rol`) VALUES
('Estudiante'),
('Programador'),
('Abogado'),
('Maestro'),
('Comerciante'),
('Contador'),
('Administrador'),
('Chófer'),
('Cantante'),
('Actor'),
('Músico'),
('Electricista '),
('Secretaria');


ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`rol`) REFERENCES `roles` (`idrol`) ON DELETE CASCADE ON UPDATE NO ACTION;

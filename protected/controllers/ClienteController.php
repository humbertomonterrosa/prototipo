<?php 

class ClienteController extends Controller{

	public $layout = 'maincustom';

	public function actionIndex(){
		$this->pageTitle = "Lista de clientes";

		$modelf = new ClienteForm('search');
		$modelf->unsetAttributes();
		if(isset($_GET['ClienteForm'])){
			$modelf->attributes=$_GET['ClienteForm'];
		}
		$model = Roles::model()->findAll();
		$this->render('index',array('model'=>$model,'modelf'=>$modelf));
	}

	public function actionView($id){
		$this->pageTitle = "Ver cliente";
		$c = Cliente::model()->findByPk($id);
		$this->render('leer',array('cliente'=>$c));
	}

	public function actionCreate(){

		$this->pageTitle = "Agregar cliente";
		$model = new ClienteForm('create');

		if(isset($_POST['ClienteForm'])){
			$model->attributes = $_POST['ClienteForm'];
			if($model->validate()){
				$c = new Cliente;
				$c->setAttributes($model->getAttributes(),false);
				$c->save();
				Yii::app()->user->setFlash('success','Cliente agregado exitosamente');
				$this->redirect(Yii::app()->controller->createUrl('/cliente'));
			}
		}

		$this->render('form',array('model'=>$model));
	}

	public function actionDelete($id){

		$c = Cliente::model()->findByPk($id);
		$c->delete();
	}

	public function actionUpdate($id){

		$this->pageTitle = "Edición de cliente";

		$c = Cliente::model()->findByPk($id);

		$model = new ClienteForm('update');
		$model->attributes = $c->getAttributes();
		if(isset($_POST['ClienteForm'])){
			$c->setAttributes($_POST['ClienteForm'],false);
			$c->save();
			Yii::app()->user->setFlash('success','Cliente actualizado');
			$this->redirect(Yii::app()->createUrl('/cliente'));
		}

		$this->render('form',array('model'=>$model));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

}
?>
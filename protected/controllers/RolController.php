<?php  

class RolController extends Controller{

	public $layout = 'maincustom';

	public function actionIndex(){
		$this->pageTitle = "Lista de roles";
		$this->render('index');
	}

	public function actionCreate(){
		$this->pageTitle = "Agregar rol";
		$model = new RolForm;

		if(isset($_POST['RolForm'])){
			$model->attributes = $_POST['RolForm'];
			if($model->validate()){
				$r = new Roles;
				$r->setAttributes($_POST['RolForm'],false);
				$r->save();
				$this->redirect(Yii::app()->controller->createUrl('/rol'));
			}
		}
		$this->render('form',array('model'=>$model));
	}

	public function actionUpdate($id){
		$this->pageTitle = "Editar rol";
		$r = Roles::model()->findByPk($id);
		$model = new RolForm;
		$model->attributes = $r->attributes;

		if(isset($_POST['RolForm'])){
			$model->attributes = $_POST['RolForm'];
			if($model->validate()){
				$r->setAttributes($_POST['RolForm'],false);
				$r->save();
				$this->redirect(Yii::app()->controller->createUrl('/rol'));
			}
		}

		$this->render('form',array('model'=>$model));

	}

	public function actionDelete($id){
		$r = Roles::model()->findByPk($id);
		$r->delete();
	}
}
?>
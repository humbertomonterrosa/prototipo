<div class="form container" role="form">
	<?php $form = $this->beginWidget('CActiveForm',array(
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true
			)
		)
	); ?>
	<div class="row">
		<div class="form-group col-lg-4">
	       <?php echo $form->label($model,'Nombre:'); ?>
	       <?php echo $form->textField($model,'nombre',array('class'=>'form-control')); ?>
	       <?php echo $form->error($model,'nombre'); ?>
	    </div>
	    <div class="form-group col-lg-4">
	        <?php echo $form->label($model,'Apellido:'); ?>
	        <?php echo $form->textField($model,'apellido',array('class'=>'form-control')); ?>
	        <?php echo $form->error($model,'apellido'); ?>
	    </div>
	    <div class="form-group col-lg-4"></div>	
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
	       <?php echo $form->label($model,'E-mail:'); ?>
	       <?php echo $form->emailField($model,'email',array('class'=>'form-control')); ?>
	       <?php echo $form->error($model,'email'); ?>
	    </div>
	    <div class="form-group col-lg-8"></div>	
	</div>

	<div class="row">
		<div class="form-group col-lg-4">
	       <?php echo $form->label($model,'Fecha de nacimiento:'); ?>
	       <?php 
			$this->widget('ext.rezvan.RDatePicker',array(
			'name'=>'ClienteForm[cumpleano]',
			'value'=>'1990-01-01',
			'htmlOptions'=>array('class'=>'form-control','type'=>'text','autocumplete'=>'off'),
			'options' => array(
			'language'=>'es',
			'format' => 'yyyy-mm-dd',
			'viewformat' => 'yyyy-mm-dd',
			'placement' => 'right',
			'todayBtn'=>false
			)
			)); 
			?>
	       <?php echo $form->error($model,'cumpleano'); ?>
	    </div>
	    <div class="form-group col-lg-8"></div>	
	</div>
	
	<div class="row">
		<div class="form-group col-lg-4">
	       <?php echo $form->label($model,'Rol:'); ?>
	       <?php echo $form->dropDownList($model,'rol',CHtml::listData(Roles::model()->findAll(),'idrol','rol'),array('prompt'=>'---Seleccione rol---','class'=>'form-control')); ?>
	       <?php echo $form->error($model,'rol'); ?>
	    </div>
	    <div class="form-group col-lg-8"></div>	
	</div>

	<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-success btn-lg')); ?> <a href="<?php echo Yii::app()->request->baseUrl; ?>" class="btn btn-default btn-lg">Cancelar</a>
	
    
    <?php $this->endWidget(); ?>
</div>
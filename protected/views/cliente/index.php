
<div>
	<a href="<?php echo Yii::app()->createUrl('/cliente/create'); ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Agregar cliente</a>
	<br /><br />
</div>
<?php if(Yii::app()->user->hasFlash('success')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo Yii::app()->user->getFlash('success'); ?>
	</div>
<?php endif; ?>
<?php 
	$form = $this->beginWidget('CActiveForm',array('method'=>'get'));
	?>
	<div class="well">
		<strong>Filtro de clientes:</strong><br /><br />
		<div class="row">
			<div class="col-md-4"><?php echo $form->textField($modelf,'nombre',array('class'=>'form-control','placeholder'=>'Nombre:')); ?></div>
			<div class="col-md-4"><?php echo $form->textField($modelf,'apellido',array('class'=>'form-control','placeholder'=>'Apellido:')); ?></div>
			<div class="col-md-4"></div>
		</div>
		<br />
		<div class="row">		
			<div class="col-md-4"><?php echo $form->dropDownList($modelf,'rol',CHtml::listData(Roles::model()->findAll(),'idrol','rol'),array('prompt'=>'---Seleccione rol---','class'=>'form-control')); ?></div>
			<div class="col-md-2"><?php echo $form->dropDownList($modelf,'fcompare',array("="=>"Igual a","<"=>"Menor que","<="=>"Menor igual que",">"=>"Mayor que",">="=>"Mayor igual que"),array('class'=>'form-control')); ?></div>
			<div class="col-md-2">
				<?php 
				$this->widget('ext.rezvan.RDatePicker',array(
				'name'=>'ClienteForm[cumpleano]',
				'value'=>$modelf->cumpleano,
				'htmlOptions'=>array('class'=>'form-control','type'=>'text','autocumplete'=>'off','placeholder'=>'Cumpleaño:'),
				'options' => array(
				'language'=>'es',
				'format' => 'yyyy-mm-dd',
				'viewformat' => 'yyyy-mm-dd',
				'placement' => 'right',
				'todayBtn'=>false
				)
				)); 
				?>
			</div>
			<div class="col-md-4"><?php echo CHtml::submitButton('Filtrar',array('class'=>'btn btn-primary')); ?></div>
		</div>
	</div>

	<?php
	$this->endWidget();

	$criteria = new CDbCriteria;
	$criteria->compare('nombre',$modelf->nombre,true);
	$criteria->compare('apellido',$modelf->apellido,true);
	if(!empty($modelf->fcompare)&&!empty($modelf->cumpleano)){
		$criteria->compare('cumpleano',$modelf->fcompare.' '.$modelf->cumpleano);
	}
	$criteria->compare('rol',$modelf->rol);
	$dataprovider = new CActiveDataProvider('Cliente',array('criteria'=>$criteria,'sort'=>array('defaultOrder'=>'concat(nombre,apellido) ASC'))); 
	$this->widget('zii.widgets.grid.CGridView',array(
		'dataProvider'=>$dataprovider,
		'columns'=>array(
			array(
				'class'=>'CButtonColumn',
				'template'=>'{view}',
				'buttons'=>array(
					'view'=>array(
						'label'=>'',
						'imageUrl'=>'',
						'options'=>array('class'=>'glyphicon glyphicon-eye-open')
						)
					)
				),
			'nombre',
			'apellido',
			'email',
			'cumpleano',
			'roles.rol',
			array('class'=>'CButtonColumn',
				'template'=>'{update} {delete}',
				'buttons'=>array(
					'update'=>array(
						'label'=>'',
						'imageUrl'=>'',
						'options'=>array('class'=>'glyphicon glyphicon-edit')
						),
					'delete'=>array(
						'label'=>'',
						'imageUrl'=>'',
						'options'=>array('class'=>'glyphicon glyphicon-remove')
						)
					)
				)
			)
		));
?>
<div class="form container" role="form">
	<?php $form = $this->beginWidget('CActiveForm',array(
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true
			)
		)
	); ?>
	<div class="row">
		<div class="control-group col-lg-4">
		<?php echo $form->label($model,'Rol:'); ?>
		<?php echo $form->textField($model,'rol',array('class'=>'form-control')) ?>
		<?php echo $form->error($model,'rol'); ?>
		</div>
	</div>
	<br />
	<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-success btn-lg')); ?>
	<a href="<?php echo Yii::app()->controller->createUrl('/rol'); ?>" class="btn btn-default btn-lg">Cancelar</a>
	<?php $this->endWidget(); ?>
</div>
<div>
	<a href="<?php echo Yii::app()->createUrl('/rol/create'); ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Agregar rol</a>
	<br /><br />
</div>

<?php $dataprovider = new CActiveDataProvider('Roles'); 
	  $this->widget('zii.widgets.grid.CGridView',array(
	  	'dataProvider'=>$dataprovider,
	  	'columns'=>array(
	  		'rol',
	  		array('class'=>'CButtonColumn',
	  			'template'=>'{update} {delete}')
	  		)
	  	)
	  );
?>
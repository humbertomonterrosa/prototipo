<?php  

class Cliente extends CActiveRecord{

	public static function model($class=__CLASS__){
		return parent::model($class);
	}
	
	public function tableName(){
		return 'cliente';
	}

	public function relations(){
		return array(
			'roles'=>array(self::BELONGS_TO,'Roles','rol')
			);
	}
}
?>
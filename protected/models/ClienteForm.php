<?php 

class ClienteForm extends CFormModel{

	public $nombre;
	public $apellido;
	public $email;
	public $cumpleano;
	public $rol;
	public $fcompare;

	public function rules(){
		return array(
			array('nombre,apellido,email,cumpleano,rol','safe','on'=>'create,update'),
			array('nombre,apellido,email,cumpleano,rol','required','on'=>'create,update'),
			array('nombre,apellido,cumpleano,rol,fcompare','safe','on'=>'search'),
			array('email','email','on'=>'create,update'),
			array('cumpleano','date','format'=>'yyyy-MM-dd','on'=>'create,update')
			);
	}
}

?>